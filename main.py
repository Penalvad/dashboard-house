import dashboard as ds

if __name__ == '__main__':

    ds.st.set_page_config('wide')

    ds.st.markdown("""
            <style>
           .css-12oz5g7 {
           max-width: 60rem;
           }
           </style>
            """,unsafe_allow_html=True)

    path='./data/house_data_finalV0.csv'
    data = ds.data_load(path)
    
    ### Dashboard selection options ###
    params=ds.set_sidebar(data)

    ### Output transformed data to show() in the dashboard ###
    dfanalysis = ds.analysis(data,params)

    ### The actual dashboard
    ds.show_table(dfanalysis['business data'],params)
    ds.show_map(dfanalysis['business data'])
    ds.price_growth_plot(dfanalysis['price growth'])
    ds.risk_selling_plot(dfanalysis['risk'])
    ds.water_front_plot(dfanalysis['wf percentage'],dfanalysis['wf table'],params)
