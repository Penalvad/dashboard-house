import pandas as  pd
import numpy as np
import streamlit as st
import geopandas as geopd
import plotly.graph_objects as go
import plotly.express as px
from folium.plugins import MarkerCluster
import folium
from streamlit_folium import folium_static

@st.cache(allow_output_mutation=True)
def data_load(path):
     return pd.read_csv(path)

@st.cache(allow_output_mutation=True)
def analysis(data,param):
    ''' Returns all the data necessary for the dashboard '''
    dfanalysis = dict()
    groups = data[['id','waterfront','date','zipcode','price','recommendation','sell price']].groupby('zipcode')
    dftest=pd.DataFrame(columns=['id','waterfront','date','zipcode','price','recommendation','sell price','margin','profit','lat','long'])
    for zipcode in set(param['zipcode']):
        dftest = pd.concat([dftest,data.loc[groups.groups[zipcode],['id','waterfront','date','zipcode','price','recommendation','sell price','sell','margin','profit','lat','long']]],axis=0)

    if param['business'] == 'buy':
        dfanalysis['business data'] = dftest.loc[(dftest['recommendation']=='buy')
                 ,['id','waterfront','date','zipcode','price','recommendation','sell price','margin','profit','lat','long']]
    elif param['business'] == 'not buy':
        dfanalysis['business data'] = dftest.loc[(dftest['recommendation']=='not buy')
                 ,['id','waterfront','date','zipcode','price','recommendation','sell price','margin','profit','lat','long']]
    elif param['business'] == 'sell':
        dfanalysis['business data'] = dftest.loc[(dftest['sell']=='sell')
                 ,['id','waterfront','date','zipcode','price','recommendation','sell price','margin','profit','lat','long']]
    elif param['business'] == 'not sell':
        dfanalysis['business data'] = dftest.loc[(dftest['sell']=='not sell')
                 ,['id','waterfront','date','zipcode','price','recommendation','sell price','margin','profit','lat','long']]

    if param['waterfront']:
        dfanalysis['business data'] = pd.concat([dfanalysis['business data'],data.loc[data['waterfront']==1,['id','date','zipcode','price','recommendation','sell price','margin','profit','lat','long']]],axis=0)

    yearly = data[['year','price','zipcode','waterfront']].groupby(['zipcode','year','waterfront'])
    years=[2014,2015]
    percentyear = {'year':[],'median price':[],'zipcode':[],'waterfront':[]}
    for (zipcode,yr,wf),frame in yearly:
        percentyear['zipcode'].append(zipcode)
        percentyear['year'].append(yr)
        percentyear['waterfront'].append(wf)
        percentyear['median price'].append(frame['price'].median())
    years = pd.DataFrame(percentyear)

# Medium average growth
    percentage={'zipcode':[],'growth':[],'waterfront':[]}
    for (zipcode,wf),frame in years.groupby(['zipcode','waterfront']):

        if len(frame['year'].unique()) == 2:
            percentage['waterfront'].append(wf)
            percentage['zipcode'].append(zipcode)
            percentage['growth'].append(frame.loc[frame['year']==2015,'median price'].iloc[0]/frame.loc[frame['year']==2014,'median price'].iloc[0])
        
    dfanalysis['price growth'] = pd.DataFrame(percentage)

# risk of selling calculus
    h72 = {'risk':[],'price_m2':[],'zipcode':[]}
    for (zipcode),frame in data[['price','sqft_lot','zipcode']].groupby(['zipcode']):
        h72['risk'].append(frame['price'].std())
        h72['price_m2'].append(frame.apply(lambda x: x['price']/x['sqft_lot'],axis=1).std())
        h72['zipcode'].append(zipcode)
    
    
    h72=pd.DataFrame(h72)    
    h72['risk']=h72['risk']/h72['risk'].max()
    dfanalysis['risk']=h72

    percent = [0.2,0.4,0.6,0.8,1.0,1.2,1.4]
    expensive = []
    median_price = data['price'].median()
    for p in percent:
        waterprice = data.loc[data['waterfront']==1,'price'].apply(lambda x: True if x >= median_price*(1.+p) else False).value_counts()
        waterprice = waterprice/data.loc[data['waterfront']==1,'waterfront'].shape[0]
        expensive.append(waterprice[True])

    dfanalysis['wf percentage'] = pd.DataFrame( data={'percent':percent, 'expensive':expensive})
    dfanalysis['wf table'] = data.loc[data['waterfront']==1,['id','date','zipcode','price','recommendation','sell price','margin','profit']]

    return dfanalysis 

def  set_sidebar(data):
    ''' Dashboard side bar, contains: zipcode area selector, business (buy,not
    buy, sell, not sell), info about water front selector 
    '''
    params = dict()
    params['zipcode']=st.sidebar.multiselect("Select Zipcode", data["zipcode"].unique(), default=data.loc[0,'zipcode'])
    params['business']=st.sidebar.selectbox("Business Decision",("buy","not buy","sell","not sell"))
    params["waterfront"]=st.sidebar.checkbox("Waterview Info")

    return params

def show_table(data,param):
    ''' show table of house business'''
    st.title("House Rocket Insights: Houses to buy and sell")
    if param['waterfront']:
        st.dataframe(data.loc[data['waterfront']==0,['id','date','zipcode','price','recommendation','sell price','margin','profit']])
    else:
        st.dataframe(data[['id','date','zipcode','price','recommendation','sell price','margin','profit']])

    st.markdown("""<style>
            .big-font{
            font-size:48px !important;
            text-align:center;
            }
            </style>
            """,unsafe_allow_html=True)
    st.markdown('<p class="big-font">{0}</p>'.format("Total Profit"),unsafe_allow_html=True)
    st.markdown('<p class="big-font">{0}</p>'.format(np.round(data['profit'].sum())),unsafe_allow_html=True)

def show_map(data):
    ''' Geolocated map with the business houses to buy or sell'''
    c1= st.checkbox("Display Map")
    density_map = folium.Map( location=[data['lat'].mean(),data['long'].mean()], default_zoom_start=15)
    marker_cluster = MarkerCluster().add_to(density_map)
    for name,row in data.iterrows():
        folium.Marker([row['lat'],row['long']], popup= "Sold {0}US$ on id {1}, date {2}, zipcode {3}".format(row['sell price'],
            row['id'],
            row['date'],
            row['zipcode'] ) ).add_to(marker_cluster)
    if c1:
        print('a')
        folium_static(density_map)

def price_growth_plot(data):
    '''Price growth'''
    fig = go.Figure()

    p=data.loc[data['waterfront']==0,['zipcode','growth']]
    fig.add_trace(go.Line(x=p['zipcode'],y=p['growth'],name="Waterfront 0"))
    p=data.loc[data['waterfront']==1,['zipcode','growth']]
    fig.add_trace(go.Scatter(mode='markers',x=p['zipcode'],y=p['growth'],name="Waterfront 1"))

    fig.update_layout(title="Yearly Median Price Growth 2014-2015",xaxis_title="Zipcode",yaxis_title="Growth",font=dict({"size":20}),width=100)
    st.plotly_chart(fig,use_container_width=True)

def risk_selling_plot(data):
    ''' Plot risk of selling'''
    fig = px.scatter(data,x='risk',y='price_m2',hover_name='zipcode',color="zipcode",width=100,title="Risk of Selling by Neighborhood")
    fig.update_layout(title="Risk of Selling by Neighborhood",xaxis_title="risk (price per neighborhood std)",yaxis_title="price_m2 std",font=dict({"size":20}))
    st.plotly_chart(fig,use_container_width=True)

def water_front_plot(data,data2,param):
    if param['waterfront']:
        c1,c2 = st.columns([1,1])
        fig = px.line(data,x="percent",y="expensive")
        fig.update_layout(title="How many water view houses are how much more expensive",xaxis_title='percent of price more expensive',yaxis_title='percent of houses water front',font=dict(size=20),width=120)
        c1.plotly_chart(fig,use_container_width=True)
        c2.dataframe(data2)
